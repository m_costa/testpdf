﻿using QuestPDF.Drawing;
using QuestPDF.Fluent;
using QuestPDF.Helpers;
using QuestPDF.Infrastructure;
using QuestPDF.Previewer;

namespace TestePDF
{
    public class TestePDF : IDocument
    {
        private readonly string _fontFamily = "Roboto";
        private readonly string _fileRegular = "RobotoCondensed-Regular-subset.ttf";
        private readonly string _fileBold = "RobotoCondensed-Bold-subset.ttf";

        public DocumentMetadata GetMetadata() => DocumentMetadata.Default;

        public void Compose(IDocumentContainer container)
        {
            FontManager.RegisterFont(GetStreamFont(_fileRegular));
            FontManager.RegisterFont(GetStreamFont(_fileBold));

            container
            .Page(page =>
            {
                page.Size(PageSizes.A4.Landscape());
                page.Margin(2, Unit.Centimetre);
                page.PageColor(Colors.White);
                page.DefaultTextStyle(x => x.FontFamily(_fontFamily));
                page.Header().Element(ComposeHeader);
                page.Content().Element(ComposeContent);
                page.Footer().Element(ComposeFooter);
            });
        }

        private void ComposeHeader(IContainer container)
        {
            container
                .Text("Hot Reload!")
                .Bold().FontColor(Colors.Blue.Darken4)
                .FontSize(50);
        }

        private void ComposeContent(IContainer container)
        {
            container.PaddingVertical(1, Unit.Centimetre).Column(x =>
            {
                x.Spacing(20);

                x.Item().DefaultTextStyle(x => x.FontSize(20)).Table(t =>
                {
                    t.ColumnsDefinition(c =>
                    {
                        c.RelativeColumn();
                        c.RelativeColumn(5);
                    });

                    t.Cell().Border(1).Background(Colors.Grey.Lighten3).Padding(5).Text("Visual Studio");
                    t.Cell().Border(1).Padding(5).Text("Start in debug mode with 'Hot Reload on Save' enabled.");
                    t.Cell().Border(1).Background(Colors.Grey.Lighten3).Padding(5).Text("Command line");
                    t.Cell().Border(1).Padding(5).Text("Run 'dotnet watch'.");
                });

                x.Item()
                    .Text("Modify this line and the preview should show your changes instantly.")
                    .FontSize(20);

            });
        }

        private void ComposeFooter(IContainer container)
        {
            container.AlignCenter().Text(x =>
            {
                x.Span("Page ");
                x.CurrentPageNumber();
                x.DefaultTextStyle(x => x.FontSize(20));
            });
        }

        public static FileStream GetStreamFont(string file) => File.OpenRead(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName, "Fonts", file));
    }

    class Program
    {
        static async Task Main(string[] args)
        {
            var document = new TestePDF();
            // document.GeneratePdf("teste.pdf");
            // var bytes = document.GeneratePdf();
            document.ShowInPreviewer();
            Task.WaitAll();
        }
    }
}