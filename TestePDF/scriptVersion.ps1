param([string]$SolutionDir, [string]$ProjectPath, [string] $PublishDir, [string]$ProjectDir)
echo('SolutionDir--> ' + $SolutionDir)
echo('ProjectPath--> ' + $ProjectPath)
echo('PublishDir--> ' + $PublishDir)
echo('ProjectDir--> ' + $ProjectDir)

$branch=(git branch --show-current).ToUpper()
if($branch -eq "MASTER" -or $branch -eq "PROD" -or $branch -eq "QA" -or $branch -eq "DEV")
{
	$date=(Get-Date -Format "MM.dd HH:mm")
	$version=-Join("Version 01.", $date, " - ", $branch)
	New-Item -Path $PublishDir -Name "version.txt" -ItemType "file" -Value $version -Force
}