param([string]$config)
echo('---> Inicio PowerShell Post-build Script')
if($config -eq "Release")
{
	echo('---> Verificar qual � a branch do c�digo')
	$branch=(git branch --show-current)
	$publishDir=('C:\Projects\Bitbucket\nlw2')
	echo('---> Verificar se o diret�rio do bin�rio existe: ' + $publishDir)
	if(Test-Path -Path $publishDir)
	{
		echo('---> Verificar se a branch do c�digo � permitida: ' + $branch)
		if($branch -eq "master" -or $branch -eq "QA" -or $branch -eq "dev")
		{
			echo('---> Ir para o diret�rio do bin�rio: ' + $publishDir)
			cd $publishDir
			$branchBin=(git branch --show-current)
			echo('---> Branch do bin�rio atual: ' + $branchBin)
			echo('---> Checkout branch do bin�rio para mesma branch do c�digo')
			git checkout $branch
			$branchBin=(git branch --show-current)
			echo('---> Branch do bin�rio ap�s checkout: ' + $branchBin)
			echo('---> Pull na branch do bin�rio: ' + $branchBin)
			git pull origin $branchBin
			echo('---> Executar o comando rm -rf * para limpar o diret�rio do bin�rio')
			& 'C:\Program Files\Git\bin\sh.exe' --login -c "rm -rf *"
			echo('---> Criar o arquivo de vers�o no diret�rio do bin�rio')
			$date=(Get-Date -Format "MM.dd HH:mm")
			$version=-Join("Version 1.", $date, " - ", $branch.ToUpper())
			New-Item -Path $publishDir -Name "version.txt" -ItemType "file" -Value $version -Force
		}
		else{echo('---> Esta branch n�o � permitida')}
	}
	else {echo('---> O diret�rio informado no script n�o existe')}

}
else {echo('---> O build n�o ocorreu na publica��o: ' + $config)}
echo('---> Fim Script')
exit