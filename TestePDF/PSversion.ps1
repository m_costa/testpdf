echo('---> Inicio Script PowerShell de cria��o do arquivo de vers�o no diret�rio de publica��o')
$publishDir=('C:\Projects\Bitbucket\nlw2')
$branch=(git branch --show-current).ToUpper()
$date=(Get-Date -Format "MM.dd HH:mm")
$version=-Join("Version 1.", $date, " - ", $branch)
New-Item -Path $publishDir -Name "version.txt" -ItemType "file" -Value $version -Force
echo('---> Fim Script')
exit