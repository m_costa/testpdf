echo('---> Inicio Script PowerShell de prepara��o do reposit�rio bin�rio antes da publica��o')
$publishDir=('C:\Projects\Bitbucket\nlw2')
$branch=(git branch --show-current)
echo('Branch do c�digo--> ' + $branch)
if($branch -eq "master" -or $branch -eq "QA" -or $branch -eq "dev")
{
	echo('Ir para o diret�rio do bin�rio--> ' + $publishDir)
	cd $publishDir
	$branchBin=(git branch --show-current)
	echo('Branch do bin�rio atual--> ' + $branchBin)
	echo('Checkout branch do bin�rio para mesma branch do c�digo')
	git checkout $branch
	$branchBin=(git branch --show-current)
	echo('Branch do bin�rio ap�s checkout--> ' + $branchBin)
	echo('Pull na branch do bin�rio--> ' + $branchBin)
	git pull origin $branchBin
	echo('Executar o comando rm -rf * para limpar o diret�rio do bin�rio')
	& 'C:\Program Files\Git\bin\sh.exe' --login -c "rm -rf *"
}
echo('---> Fim Script')
exit